<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
//Usar en todos los controllers para hacer funcionar el update 
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataProducts['products']=Product::paginate(7);
        return view('products.index',$dataProducts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataProducts= request()->except('_token');

        if($request->hasFile('picture')){
            $dataProducts['picture']=$request->file('picture')->store('uploads','public');
        }

        Product::insert($dataProducts);
        return redirect('products')->with('message','Producto agregado con éxito');
        //return response()->json($dataProducts);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::findOrFail($id);
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       //
        $dataProducts= request()->except(['_token','_method']);

        if($request->hasFile('picture')){
            $product=Product::findOrFail($id);
            Storage::delete(['products'.$product->picture]);
            $dataProducts['picture']=$request->file('picture')->store('uploads','public');
        }

        Product::where('id','=',$id)->update($dataProducts);

        $product=Product::findOrFail($id);
        return view('products.edit', compact('product'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $product=Product::findOrFail($id);
        if(Storage::delete('public/'.$product->picture)){
            //Borrar la foto mediante el id
            Product::destroy($id);
        }
        
        return redirect('products')->with('message','Product deleted successfully');
    }
}
