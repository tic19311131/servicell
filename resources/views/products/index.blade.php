@if (Session::has('message'))
    {{ Session::get('message') }}
@endif
<h1>Products</h1>
<a href="{{ url('products/create') }}">Registrar Producto</a>
<table class="table table-dark">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Description</th>
            <th>Color</th>
            <th>Price</th>
            <th>Picture</th>
            <th>Codebar</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->color }}</td>
            <td>{{ $product->price }}</td>
            <td>
                <img src="{{ asset('storage').'/'.$product->picture  }}" width="150" alt="">
            </td>
            <td>{{ $product->codebar }}</td>
            <td>
                <a href="{{ url('/products/'.$product->id.'/edit') }}">
                    Update
                </a> |  
                
                <form action="{{ url('/products/'.$product->id) }}" method="post">
                @csrf
                {{ method_field('DELETE') }}
                <input type="submit"  onclick="return confirm('¿Deseas borrar este producto?')" value="DELETE">
               </form>
        @endforeach
    </tbody>
</table>