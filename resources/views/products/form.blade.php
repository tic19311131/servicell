    <h1>{{ $mode }} Product</h1>
    <label for="name">Name</label>
    <input type="text" name="name" value="{{ isset($product->name)?$product->name:'' }}" id="name"><br>

    <label for="description">Description</label>
    <input type="text" name="description" value="{{ isset($product->description)?$product->description:'' }}" id="description"><br>

    <label for="color">Color</label>
    <input type="text" name="color" value="{{ isset($product->color)?$product->color:'' }}" id="color"><br>

    <label for="price">Price</label>
    <input type="text" name="price" value=" {{ isset($product->name)?$product->price:'' }}" id="price"><br>

    <label for="picture">Picture</label><br>
    @if (isset($product->picture))
    <img src="{{ asset('storage').'/'.$product->picture  }}" width="150" alt=""><br>
    @endif
    <input type="file" name="picture" value="" id="picture"><br>

    <label for="codebar">Codebar</label>
    <input type="text" name="codebar"   value=" {{ isset($product->codebar)?$product->codebar:''  }}" id="codebar"><br>

    <input type="submit" value="{{ $mode }} Product"><br>
    <a href="{{ url('products') }}">Back</a>